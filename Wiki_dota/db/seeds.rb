# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


  users1 = User.create(id: 1, email: "romeucarvalho2009@hotmail.com", admin: true, first_name: "Romeu", last_name: "Antunes", password: "33825791")
