class AddHeroIdToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :hero_id, :integer
  end
end
