class CreateHeros < ActiveRecord::Migration
  def change
    create_table :heros do |t|
      t.string :name
      t.string :main_att
      t.string :str_base
      t.string :int_base
      t.string :agi_base

      t.timestamps
    end
  end
end
